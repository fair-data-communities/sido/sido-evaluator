require "spec_helper"

describe "SidoPlanningsEvaluator2020", type: :model do
  include EvaluatorSetup
  include PlanningEvaluator

  it "must evaluate simple file" do
    # Given
    setup_evaluator(
      fixture: "simple_metadata.xml"
    )

    # When
    results = context.execute(variables)

    # Then -- result
    expect(feedback_contains_errors?(results)).to eq false
    rulings = results.perform_rulings
    puts rulings.inspect
  end
end
