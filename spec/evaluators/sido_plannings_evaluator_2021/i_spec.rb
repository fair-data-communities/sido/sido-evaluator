require "spec_helper"

context "SidoPlanningsEvaluator2020", type: :model do
  include EvaluatorSetup
  include PlanningEvaluator

  describe "Accessable" do
    it "must give full points" do
      # Given
      setup_evaluator fixture: "plannings_all.xml"

      # When
      results = context.execute(variables)
      results.base_metrics = ["I"]

      # Then
      rulings = results.perform_rulings
      expect(feedback_contains_errors?(results)).to eq false
      expect(rulings["I"]).to eq 1.0
    end

    it "I1: metadata does not conform to 'plannings' definition" do
      # Given
      setup_evaluator fixture: "no_valid_dataset.xml"

      # When
      results = context.run_only(:I1, variables)

      # Then
      value = results.results.values[0]
      expect(feedback_contains_errors?(results)).to eq false
      expect(value).to eq 0.0
      expect(results.feedback.count).to eq 1
      expect(results.feedback[0][:type]).to eq :recommendation
      expect(results.feedback[0][:comment]).to eq :conform_to_sido_planning
    end

    it "I2: mapping to SidO vocabulary" do
      # Given
      setup_evaluator fixture: "plannings_sido3.xml"

      # When
      results = context.execute(variables)

      # Then
      value = results.results.find { |key, _| key.name == "SidO" }[1]
      expect(feedback_contains_errors?(results)).to eq false
      expect((value * 70).to_i).to eq 22
      expect(results.feedback.count).to eq 16
      expect(results.feedback[1][:type]).to eq :recommendation
      expect(results.feedback[1][:comment]).to eq :missing_mapping
    end
  end
end
