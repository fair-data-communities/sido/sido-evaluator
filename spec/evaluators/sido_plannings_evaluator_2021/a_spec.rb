require "spec_helper"

context "SidoPlanningsEvaluator2020", type: :model do
  include EvaluatorSetup
  include PlanningEvaluator

  describe "Accessable" do
    it "must give full points" do
      # Given
      setup_evaluator

      # When
      results = context.run_only(:A, variables)
      results.base_metrics = ["A"]

      # Then
      rulings = results.perform_rulings
      expect(feedback_contains_errors?(results)).to eq false
      expect(rulings["A"]).to eq 1.0
    end

    it "A1: document does not contains XMl/RDF" do
      # Given
      setup_evaluator

      # When
      results = context.run_only(:A1, variables)

      # Then
      value = results.results.values[0]
      expect(feedback_contains_errors?(results)).to eq false
      expect(value).to eq 1.0
    end

    it "A3: combined score" do
      # Given
      setup_evaluator(fixture: "plannings_a3.xml")

      # When
      results = context.run_only(:"A3", variables)

      # Then
      value = results.results.values[0]
      expect(feedback_contains_errors?(results)).to eq false
      expect(value).to eq 0
      expect(results.feedback.count).to eq 3
    end

    it "A3.1: server has insufficient capacity" do
      # Given
      setup_evaluator(fixture: "plannings_a31.xml")

      # When
      results = context.run_only(:"A3.1", variables)

      # Then
      value = results.results.values[0]
      expect(feedback_contains_errors?(results)).to eq false
      expect(value).to eq 0.0
      expect(results.feedback.count).to eq 1
      expect(results.feedback[0][:type]).to eq :recommendation
      expect(results.feedback[0][:comment]).to eq :server_capacity_no
    end

    it "A3.2: server has insufficient uptime" do
      # Given
      setup_evaluator(fixture: "plannings_a32.xml")

      # When
      results = context.run_only(:"A3.2", variables)

      # Then
      value = results.results.values[0]
      expect(feedback_contains_errors?(results)).to eq false
      expect(value).to eq 0.0
      expect(results.feedback.count).to eq 1
      expect(results.feedback[0][:type]).to eq :recommendation
      expect(results.feedback[0][:comment]).to eq :server_uptime_no
    end

    it "A3.3: server restore takes a long time" do
      # Given
      setup_evaluator(fixture: "plannings_a33.xml")

      # When
      results = context.run_only(:"A3.3", variables)

      # Then
      value = results.results.values[0]
      expect(feedback_contains_errors?(results)).to eq false
      expect(value).to eq 0
      expect(results.feedback.count).to eq 1
      expect(results.feedback[0][:type]).to eq :recommendation
      expect(results.feedback[0][:comment]).to eq :reduce_server_restore_time
    end
  end
end
