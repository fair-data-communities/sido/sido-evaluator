require "spec_helper"

context "SidoAssetsEvaluator2020", type: :model do
  include EvaluatorSetup
  include AssetEvaluator

  describe "SidO compliancy" do
    it "must give full points" do
      # Given
      setup_evaluator fixture: "assets_sido5_full.xml"

      # When
      results = context.run_only(:SidO, variables)
      results.base_metrics = ["SidO"]

      # Then
      rulings = results.perform_rulings
      expect(feedback_contains_errors?(results)).to eq false
      puts results.feedback if results.feedback.length.positive?
      expect(rulings["SidO"]).to eq 1.0
    end

    it "SidO1: data protocol information was not provided" do
      # Given
      setup_evaluator fixture: "assets_sido1.xml"

      # When
      results = context.run_only(:"SidO1", variables)

      # Then
      value = results.results.values[0]
      expect(feedback_contains_errors?(results)).to eq false
      expect(value).to eq 0.0
      expect(results.feedback.count).to eq 1
      expect(results.feedback[0][:type]).to eq :recommendation
      expect(results.feedback[0][:comment]).to eq :no_valid_data_protocol
    end

    it "SidO2: no security mechanism" do
      # Given
      setup_evaluator fixture: "assets_sido2.xml"

      # When
      results = context.run_only(:"SidO2", variables)

      # Then
      value = results.results.values[0]
      expect(feedback_contains_errors?(results)).to eq false
      expect(value).to eq 1.0
      expect(results.feedback.count).to eq 0
    end

    it "SidO2: security mechanism is not available" do
      # Given
      setup_evaluator fixture: "simple_metadata.xml"

      # When
      results = context.run_only(:"SidO2", variables)

      # Then
      value = results.results.values[0]
      expect(feedback_contains_errors?(results)).to eq false
      expect(value).to eq 0.0
      expect(results.feedback.count).to eq 1
      expect(results.feedback[0][:type]).to eq :recommendation
      expect(results.feedback[0][:comment]).to eq :no_valid_security_mechanism
    end

    it "SidO3: missing mappings" do
      # Given
      setup_evaluator fixture: "assets_sido3.xml"

      # When
      results = context.run_only(:"SidO3", variables)

      # Then
      value = results.results.values[0]
      expect(feedback_contains_errors?(results)).to eq false
      expect(value).to eq 2 / 6.0
      expect(results.feedback.count).to eq 4
      expect(results.feedback[0][:type]).to eq :recommendation
      expect(results.feedback[0][:comment]).to eq :missing_mapping
      expect(results.feedback[0][:vars][:name]).to eq "curve"
      expect(results.feedback[1][:type]).to eq :recommendation
      expect(results.feedback[1][:comment]).to eq :missing_mapping
      expect(results.feedback[1][:vars][:name]).to eq "name"
      expect(results.feedback[2][:type]).to eq :recommendation
      expect(results.feedback[2][:comment]).to eq :missing_mapping
      expect(results.feedback[2][:vars][:name]).to eq "asset"
      expect(results.feedback[3][:type]).to eq :recommendation
      expect(results.feedback[3][:comment]).to eq :missing_mapping
      expect(results.feedback[3][:vars][:name]).to eq "material"
    end

    it "SidO4: license not accepted" do
      # Given
      setup_evaluator fixture: "simple_metadata.xml"

      # When
      results = context.run_only(:"SidO4", variables)

      # Then
      value = results.results.values[0]
      expect(feedback_contains_errors?(results)).to eq false
      expect(value).to eq 0
      expect(results.feedback.count).to eq 1
      expect(results.feedback[0][:type]).to eq :recommendation
      expect(results.feedback[0][:comment]).to eq :license_not_accepted
    end

    it "SidO5: everything ok" do
      # Given
      setup_evaluator fixture: "assets_sido5.xml"

      # When
      results = context.run_only(:"SidO5", variables)

      # Then
      value = results.results.values[0]
      expect(feedback_contains_errors?(results)).to eq false
      expect(value).to eq 1
      expect(results.feedback.count).to eq 0
    end

    it "SidO5: metadata missing" do
      # Given
      setup_evaluator fixture: "simple_metadata.xml"

      # When
      results = context.run_only(:"SidO5", variables)

      # Then
      value = results.results.values[0]
      expect(feedback_contains_errors?(results)).to eq false
      expect(value).to eq 0
      expect(results.feedback.count).to eq 4
      expect(results.feedback[0][:type]).to eq :recommendation
      expect(results.feedback[0][:comment]).to eq :dataset_missing_functionally_responsible_block
      expect(results.feedback[1][:type]).to eq :recommendation
      expect(results.feedback[1][:comment]).to eq :dataset_missing_technically_responsible_block
      expect(results.feedback[2][:type]).to eq :recommendation
      expect(results.feedback[2][:comment]).to eq :dataset_missing_available_until
      expect(results.feedback[3][:type]).to eq :recommendation
      expect(results.feedback[3][:comment]).to eq :dataset_missing_epsg7415_compliancy
    end
  end
end
