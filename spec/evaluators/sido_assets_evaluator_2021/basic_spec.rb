require "spec_helper"

describe "SidoAssetsEvaluator2020", type: :model do
  include EvaluatorSetup
  include AssetEvaluator

  it "must evaluate simple file" do
    # Given
    setup_evaluator(
      fixture: "simple_metadata.xml"
    )

    # When
    results = context.execute(variables)

    # Then -- result
    # Since this file misses about everything, all scores must be minimal
    expect(feedback_contains_errors?(results)).to eq false
    rulings = results.perform_rulings
    puts rulings.inspect
  end
end
