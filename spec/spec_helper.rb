require "sorbet"
require "nokogiri"

# Use memory cache
Sorbet::Config::Configuration.cache = Sorbet::Evaluators::MemCache.new
Sorbet::Config::Configuration.logger = Sorbet::Logger::NoLogger.new

# TODO: load default evaluator
require "~/purple_polar_bear/fairbear/default_evaluators/spec/spec_helper"

# Load services
Dir[File.join(__dir__, "../services/**/*.rb")].each { |f| require f }
# Load singletons
Sorbet::DefaultEvaluators::HttpSorbet.singleton = Sorbet::DefaultEvaluators::HttpTestSorbet.new

# Support
Dir[File.join(__dir__, "./support/**/*.rb")].each { |f| require f }

# Support methods
def mock_specification(evaluator, specification_name, ruby)
  evaluator.specifications.delete_if { |spec| spec.name.to_s == specification_name.to_s }
  evaluator.specifications << Sorbet::Model::Specification.new(
    name: specification_name,
    ruby: ruby
  )
end

def feedback_contains_errors?(results)
  error = results
    .feedback
    .find { |feedback| feedback[:type] == :error }

  return false if error.nil?

  puts error[:backtrace]
  puts error[:error_message]
  true
end
