require "spec_helper"

describe Sorbet::DefaultEvaluators::Metadata do
  it "must process simple metadata file" do
    # Given
    url = "http://purplepolarbear.nl/metadata"
    body = File.read(File.join(__dir__, "../fixtures/metadata.xml"))
    Sorbet::DefaultEvaluators::HttpSorbet.instance.setup(url: url, body: body)

    # When
    metadata = Sorbet::DefaultEvaluators::Metadata.new
    metadata.load_metadata(url)

    # Then
    expect(metadata.all_datasets.count).to eq 2
  end
end
