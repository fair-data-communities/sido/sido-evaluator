require "spec_helper"

describe Sorbet::DefaultEvaluators::AssetMetadata do
  it "must process simple asset metadata file" do
    # Given
    url = "http://purplepolarbear.nl/metadata"
    body = File.read(File.join(__dir__, "../fixtures/metadata.xml"))
    Sorbet::DefaultEvaluators::HttpSorbet.instance.setup(url: url, body: body)

    # When
    metadata = Sorbet::DefaultEvaluators::AssetMetadata.new
    metadata.load_metadata(url)

    # Then - datasets
    expect(metadata.all_datasets.count).to eq 2
    expect(metadata.datasets.count).to eq 1
    expect(metadata.dataset).to eq metadata.datasets[0][:object]
  end

  it "must process asset metadata file" do
    # Given
    url = "http://purplepolarbear.nl/metadata"
    body = File.read(File.join(__dir__, "../fixtures/metadata.xml"))
    Sorbet::DefaultEvaluators::HttpSorbet.instance.setup(url: url, body: body)

    # When
    metadata = Sorbet::DefaultEvaluators::AssetMetadata.new
    metadata.load_metadata(url)

    # Then - results
    server_info = metadata.dataset.server_info
    expect(server_info[:capacity]).to eq :yes
    expect(server_info[:uptime]).to eq :yes
    expect(server_info[:restore]).to eq 24
  end

  it "must process empty asset metadata file" do
    # Given
    url = "http://purplepolarbear.nl/metadata"
    body = File.read(File.join(__dir__, "../fixtures/simple_metadata.xml"))
    Sorbet::DefaultEvaluators::HttpSorbet.instance.setup(url: url, body: body)

    # When
    metadata = Sorbet::DefaultEvaluators::AssetMetadata.new
    metadata.load_metadata(url)

    # Then - results
    server_info = metadata.dataset.server_info
    expect(server_info[:capacity]).to eq :unknown
    expect(server_info[:uptime]).to eq :unknown
    expect(server_info[:restore]).to eq :unknown
  end

  it "must process empty identification info" do
    # Given
    url = "http://purplepolarbear.nl/metadata"
    body = File.read(File.join(__dir__, "../fixtures/no_valid_dataset.xml"))
    Sorbet::DefaultEvaluators::HttpSorbet.instance.setup(url: url, body: body)

    # When
    metadata = Sorbet::DefaultEvaluators::AssetMetadata.new
    metadata.load_metadata(url)

    # Then - results
    identification_info = metadata.identification_info
    expect(identification_info[:dataset_present]).to eq true
    expect(identification_info[:valid_dataset_present]).to eq false
    expect(identification_info[:distribution_present]).to eq false
    expect(identification_info[:distribution_contains_url]).to eq false
  end
end
