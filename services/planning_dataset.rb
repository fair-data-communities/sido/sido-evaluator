require_relative "./rdf_name"
require_relative "./namespaces"
require_relative "./planning_distribution"
require_relative "./validations"

module Sorbet
  module DefaultEvaluators
    module PlanningDataset
      include SidODefinitions
      include StandardNamespaces
      include Validations

      attr_accessor :metadata

      # At least one dc:conformsto is present with the given name.
      def conforms_to(local)
        conformsTo = RdfName.new(space: NS_DC, local: 'conformsTo')
        localName = RdfName.new(space: METADATA_PLANNING, local: local)
        match_names(conformsTo, localName)
      end

      def legal_info
        {
          sido_2020: sido_license_present?
        }
      end

      def data_info
        result = {}

        available_until = contains_predicate(RdfName.new(space: METADATA_PLANNING, local: "AvailableUntil"))
        result[:available_until] = valid_value?(available_until&.name&.value)
        epsg7415_compliant = contains_predicate(RdfName.new(space: METADATA_PLANNING, local: "Epsg7415Compliant"))
        result[:epsg7415_compliant] = valid_value?(epsg7415_compliant&.name&.value)

        result
      end

      def server_info
        return @server_info if @server_info
        {
          capacity: server_capacity,
          uptime: server_uptime,
          restore: server_restore
        }
      end

      def security_info
        results = {
          mechanism: :unknown
        }

        results[:mechanism] = :client_server_ssl if match_names(sidoPlanningTerm('Authentication'), sidoPlanningTerm('Authentication-clientserverssl'))
        results[:mechanism] = :server_ssl if match_names(sidoPlanningTerm('Authentication'), sidoPlanningTerm('Authentication-none'))

        results
      end

      # Returns the distributions
      def distributions
        return @distributions if @distributions

        distribution_predicate = RdfName.new(space: NS_DCAT, local: 'distribution')
        distribution_list = contains_predicate(distribution_predicate)
        if distribution_list.nil?
          @distributions = []
          return
        end

        distribution_item_predicate = RdfName.new(space: NS_DCAT, local: 'Distribution')
        @distributions = distribution_list.contains_predicate_list(distribution_item_predicate)
        @distributions = @distributions.map do |distribution|
          distribution = metadata.resolve(distribution)
          distribution.extend PlanningDistribution
          distribution.dataset = self
          distribution
        end
        @distributions
      end

      # Currently, only one distribution is supported
      def distribution
        distributions&.first
      end

      def provenance_info
        result = {
          functional: {},
          technical: {}
        }

        publisher_predicate = RdfName.new(space: NS_DC, local: 'publisher')
        publisher = contains_predicate(publisher_predicate)
        label_predicate = RdfName.new(space: NS_RDFS, local: 'label')
        label = publisher&.contains_predicate(label_predicate)
        contributor_predicate = RdfName.new(space: NS_DC, local: 'contributor')
        contributor = contains_predicate(contributor_predicate)
        result[:organisation] = valid_value?(label&.name&.value)
        result[:author] = valid_value?(contributor&.name&.value)

        functional_predicate = RdfName.new(space: METADATA_PLANNING, local: 'FunctionallyResponsible')
        foaf = contains_predicate(functional_predicate)
        if foaf
          foaf = metadata.resolve(foaf)
          result[:functional][:block] = true
          name = foaf.contains_predicate(RdfName.new(space: NS_FOAF, local: 'name'))
          result[:functional][:name] = valid_value?(name&.name&.value)
          email = foaf.contains_predicate(RdfName.new(space: NS_FOAF, local: 'mbox'))
          result[:functional][:email] = valid_value?(email&.name&.value)
        end

        technical_predicate = RdfName.new(space: METADATA_PLANNING, local: 'TechnicallyResponsible')
        foaf = contains_predicate(technical_predicate)
        if foaf
          foaf = metadata.resolve(foaf)
          result[:technical][:block] = true
          name = foaf.contains_predicate(RdfName.new(space: NS_FOAF, local: 'name'))
          result[:technical][:name] = valid_value?(name&.name&.value)
          email = foaf.contains_predicate(RdfName.new(space: NS_FOAF, local: 'mbox'))
          result[:technical][:email] = valid_value?(email&.name&.value)
        end

        result
      end

      private

      def server_capacity
        return :yes if match_names(sidoPlanningTerm('ServerCapacity'), sidoPlanningTerm('ServerCapacity-yes'))
        return :no if match_names(sidoPlanningTerm('ServerCapacity'), sidoPlanningTerm('ServerCapacity-no'))
        :unknown
      end

      def server_uptime
        return :yes if match_names(sidoPlanningTerm('ServerUptime'), sidoPlanningTerm('ServerUptime-yes'))
        return :no if match_names(sidoPlanningTerm('ServerUptime'), sidoPlanningTerm('ServerUptime-no'))
        :unknown
      end

      def server_restore
        return :yes if match_names(sidoPlanningTerm('ServerRestore'), sidoPlanningTerm('ServerRestore-yes'))
        return :no if match_names(sidoPlanningTerm('ServerRestore'), sidoPlanningTerm('ServerRestore-no'))
        :unknown
      end

      def sido_license_present?
        return @sido_license_present if @sido_license_present
        licensePredicate = RdfName.new(space: NS_DC, local: 'license')
        license = RdfName.new(space: "https://gitlab.com/fair-data-communities/sido/assets/-/raw/master/licenses/user_license.md")
        @sido_license_present = match_names(licensePredicate, license)
      end
    end
  end
end
