module Sorbet
  module DefaultEvaluators
    class RdfNode
      attr_accessor :default_namespace

      attr_accessor :name

      # Children of the RdfNode
      attr_accessor :nodes

      attr_accessor :resource_node

      attr_accessor :value_node

      def initialize(name:, resource_node: false, value_node: false)
        self.name = name
        self.resource_node = resource_node
        self.value_node = value_node
        self.nodes = []
      end

      # Returns whether a predicate is present at least once. This function
      # returns the object of the given predicate.
      def contains_predicate(predicate)
        node = nodes.find { |node| node[:predicate].name.equals(predicate) }

        return nil if node.nil?
        node[:object]
      end

      def contains_predicate_list(predicate)
        nodes
          .select { |node| node[:predicate].name.equals(predicate) }
          .map { |node| node[:object] }
      end

      # Returns whether a predicate with the given object is present at least
      # once. This function always returns a boolean.
      def match_names(predicate, object)
        nodes.any? do |node|
          next false unless node[:predicate].name.equals(predicate)

          # match if string values match
          node[:object].name.equals(object)
        end
      end

      # Returns the current node and the children
      def node_to_s(indent: 0)
        result = ""
        nodes.each do |node|
          result += ("  " * indent) + "(#{node[:predicate].to_s}) -> (#{node[:object].to_s})\n"
          result += node[:object].node_to_s(indent: indent + 1)
        end
        result
      end

      # Returns the current node
      def to_s
        name.to_s
      end
    end
  end
end
