require_relative "./rdf_node"
require_relative "./rdf_document"
require_relative "./namespaces"

module Sorbet
  module DefaultEvaluators
    class Metadata < RdfNode
      include Sorbet::Evaluators::Service
      include RdfDocument
      include StandardNamespaces

      attr_accessor :file_status

      FILE_STATUS_OK      = "ok"
      FILE_STATUS_FAILED  = "failed"

      def initialize
        # Name is set by the configure method
        super(name: RdfName.new)
      end

      def configure(configuration)
        # load_metadata(configuration[:metadata_url])
      end

      def load_metadata(url)
        result = HttpSorbet.get(url)
        if (result[:code] / 100) != 2
          @file_status = FILE_STATUS_FAILED
          @nodes = []
          return
        end

        @file_status = FILE_STATUS_OK
        parsed_document = parse_rdf_document!(result[:body])
      end

      def all_datasets
        @all_datasets ||= types(RdfName.new(space: NS_DCAT, local: 'Dataset'))
      end

      # Returns all the RDF entities that match a certain type
      def types(type_name, list = nil)
        predicate = RdfName.new(space: NS_DC, local: 'type')
        list ||= nodes
        list.select do |node|
          node[:object].match_names(predicate, type_name)
        end
      end

      def protocol_info
        {
          application_layer: :https,
          file_format: :rdf_xml
        }
      end

      def resolve(node)
        if node.resource_node && node.name.space == ""
          nodes.each do |document_node|
            object = document_node[:object]
            if object.name.local == node.name.local
              return object
            end
          end
        end

        return node
      end

      # Returns a single RDF entitity that matches the given name. If the node
      # cannot be found, the function returns nil.
      def by_name(local)
        nil
      end

      # list of expected mappings to be present. Mappings are encoded
      # as their local names
      def expected_mapping
        []
      end

      # Namespace of the mappings
      def mapping_space
        ""
      end

      # Returns all the valid datasets. A valid dataset is a dataset which
      # has the following properties:
      # - conforms to appropriate definition
      def datasets
        unless respond_to?(:dataset_name)
          all_datasets
        end

        types(dataset_name, all_datasets)
      end

      # Returns the first dataset of the datasets.
      def dataset
        return @dataset if @dataset

        @dataset = (datasets&.first&.dig(:object) || RdfNode.new(name: RdfName.new()))
        extend_dataset(@dataset) if respond_to?(:extend_dataset)
        @dataset.metadata = self
        @dataset
      end

      # Returns the mapping info
      def mappings_info
        expected = expected_mapping
        found = []

        owl_class_predicate = RdfName.new(space: NS_OWL, local: "Class")
        owl_sameas_predicate = RdfName.new(space: NS_OWL, local: "sameAs")
        ppb_constant_predicate = RdfName.new(space: NS_PPB, local: "constant")
        type_predicate = RdfName.new(space: NS_RDF, local: "type")
        dataset.metadata.nodes.each do |node|
          if node[:predicate].name.equals(owl_class_predicate)
            owl_class = node[:object]

            # mapping of owl:Class to field
            sameas_list = owl_class.contains_predicate_list(owl_sameas_predicate)
            sameas_list.each do |sameas|
              if sameas.name.space == mapping_space
                name = sameas.name.local
                found << name if expected.include?(name)
              end
            end

            # mapping of constant to field
            ppb_constants = owl_class.contains_predicate_list(ppb_constant_predicate)
            ppb_constants.each do |ppb_constant|
              type_value = ppb_constant.contains_predicate(type_predicate)
              if type_value && type_value.name.space == mapping_space
                name = type_value.name.local
                found << name if expected.include?(name)
                # TODO: klopt de resource waarde wel?
              end
            end
          end
        end

        result = {
          mandatory_found: found.length,
          mandatory_total: expected.length,
          missing: [
          ]
        }

        (expected - found).each do |name|
          result[:missing] << {
            name: name,
            mandatory: true
          }
        end

        result
      end
    end
  end
end
