module Sorbet
  module DefaultEvaluators
    # An RDF name. The name can be a namespace (URL), a local name without a
    # namespace or a name containing both.
    #
    #
    class RdfName
      # Name of the node
      attr_reader :local

      # Namespace of the node
      attr_reader :space

      def initialize(space: nil, local: nil, fullname: nil)
        update_name(space: space, local: local, fullname: fullname)
      end

      def value
        fullname
      end

      def fullname
        return @fullname if @fullname

        result = []
        result << space if space && (space != "")
        result << local if local && (local != "")
        @fullname = result.join("#")
      end

      def update_name(space: nil, local: nil, fullname: nil)
        result_space, result_local = build_space_local(space, local, fullname)
        @space = result_space || ""
        @local = result_local || ""
      end

      def build_space_local(space, local, fullname)
        if fullname
          space, local = fullname.split("#")
          return space, local if local
          return space, local if space&.start_with?("http")
          return "", space
        end

        return space&.split("#")&.first, local
      end

      # Returns whether this node equals a specific name
      def equals(other)
        return (space == other.space) && (local == other.local)
      end

      # Returns the current node
      def to_s
        return space if local.blank?
        return local if space.blank?

        "#{space}##{local}"
      end
    end
  end
end
