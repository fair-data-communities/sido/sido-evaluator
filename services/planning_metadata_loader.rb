require_relative "./planning_metadata"

module Sorbet
  module DefaultEvaluators
    class PlanningMetadataLoader
      attr_accessor :cache

      def configure(_)
        self.cache = {}
      end

      def load(url)
        return cache[url] if cache[url]

        result = PlanningMetadata.new
        result.load_metadata(url)
        cache[url] ||= result
      end
    end
  end
end
