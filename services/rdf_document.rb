require_relative("./rdf_name")
require_relative("./rdf_node")
require_relative("./rdf_value")

module Sorbet
  module DefaultEvaluators
    module RdfDocument
      def parse_rdf_document!(text)
        doc = Nokogiri::XML(text)
        self.name = RdfName.new(
          local: doc.root.name,
          space: doc.url
        )
        parse_node(self, doc.root)
      end

      def parse_node(subject, xml_node)
        nodes = []
        predicate_rdf_resource = RdfName.new(space: StandardNamespaces::NS_RDF, local: "resource")
        predicate_rdf_about = RdfName.new(space: StandardNamespaces::NS_RDF, local: "about")
        predicate_rdf_id = RdfName.new(space: StandardNamespaces::NS_RDF, local: "ID")
        xml_node.attributes.each do |_, attribute|
          space = attribute.namespace.href if attribute.namespace
          predicate = RdfNode.new(name: RdfName.new(space: space, local: attribute.name))

          if predicate.name.equals(predicate_rdf_resource)
            subject.name.update_name(fullname: attribute.value)
            subject.resource_node = true
            next
          end

          if predicate.name.equals(predicate_rdf_about)
            subject.name.update_name(fullname: attribute.value)
            subject.resource_node = true
            next
          end

          if predicate.name.equals(predicate_rdf_id)
            subject.name.update_name(space: name.space, local: attribute.value)
            subject.resource_node = true
            next
          end

          object = RdfNode.new(name: RdfName.new(fullname: attribute.value))
          new_node = {
            predicate: predicate,
            object: object
          }
          subject.nodes << new_node
        end

        xml_node.children.each do |child|
          if child.is_a?(Nokogiri::XML::Text)
            text = child.to_s
            next if text.strip == ""

            subject.name = RdfValue.new(child.to_s)
            subject.value_node = true
            next
          end

          space = child.namespace.href if child.namespace
          predicate = RdfNode.new(name: RdfName.new(space: space, local: child.name))
          value = ""
          object = RdfNode.new(name: RdfName.new(local: value))

          new_node = {
            predicate: predicate,
            object: object
          }
          subject.nodes << new_node
          parse_node(object, child)
        end
      end
    end
  end
end
