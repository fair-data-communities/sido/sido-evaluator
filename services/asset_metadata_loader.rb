require_relative "./asset_metadata"

module Sorbet
  module DefaultEvaluators
    class AssetMetadataLoader
      attr_accessor :cache

      def configure(_)
        self.cache = {}
      end

      def load(url)
        return cache[url] if cache[url]

        result = AssetMetadata.new
        result.load_metadata(url)
        cache[url] ||= result
      end
    end
  end
end
