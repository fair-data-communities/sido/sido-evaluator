module Sorbet
  module DefaultEvaluators
    class UuidIdentifier
      include Sorbet::Evaluators::Service

      UUID_PATTERN = "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}"
      URL_FORM = "^https://((.*)+)$"

      def configure(configuration)
      end

      def match(url)
        return :fail if url.nil?

        match_data = url.match(URL_FORM)
        if match_data.nil?
          return :fail
        end

        if match_data[1].match(UUID_PATTERN)
          return :pass
        end

        :fail
      end
    end
  end
end
