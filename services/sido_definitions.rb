require_relative "./rdf_name"

module Sorbet
  module DefaultEvaluators
    module SidODefinitions
      METADATA_SIDO_URL         = 'https://gitlab.com/fair-data-communities/sido/assets/-/raw/master/definitions/'
      METADATA_ASSET_DEFINITION = METADATA_SIDO_URL + 'assets.rdf'
      METADATA_ASSET            = METADATA_SIDO_URL + 'assets.rdf'
      METADATA_PLANNING         = METADATA_SIDO_URL + 'plannings.rdf'
      METADATA_ASSET_MAPPINGS   = METADATA_SIDO_URL + 'asset_md.rdf'
      METADATA_PLANNING_MAPPINGS= METADATA_SIDO_URL + 'planning_md.rdf'

      def sidoAssetTerm(local)
        RdfName.new(space: METADATA_ASSET, local: local)
      end

      def sidoPlanningTerm(local)
        RdfName.new(space: METADATA_PLANNING, local: local)
      end
    end
  end
end
