module Sorbet
  module DefaultEvaluators
    # An RDF name. The name can be a namespace (URL), a local name without a
    # namespace or a name containing both.
    #
    #
    class RdfValue
      attr_accessor :value

      def initialize(value)
        self.value = value
      end
    end
  end
end
