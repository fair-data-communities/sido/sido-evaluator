require_relative "./metadata"
require_relative "./sido_definitions"
require_relative "./asset_dataset"
require_relative "./validations"

module Sorbet
  module DefaultEvaluators
    # Interpretes metadata files. For now, this is more of an XML parser
    # than a RDF parser. However, this behavious may change in the future.
    class AssetMetadata < Metadata
      include SidODefinitions
      include Validations

      def dataset_name
        RdfName.new(space: METADATA_ASSET, local: "")
      end

      def extend_dataset(dataset)
        dataset.extend AssetDataset
      end

      def expected_mapping
        ["id", "curve", "name", "asset", "material", "outerdiameter"]
      end

      def mapping_space
        METADATA_ASSET_MAPPINGS
      end

      def identification_info
        {
          dataset_present: all_datasets.count.positive?,
          valid_dataset_present: datasets.count.positive?,
          distribution_present: dataset.distributions&.count&.positive? || false,
          distribution_contains_url: valid_value?(dataset.distribution&.endpoint_url)
        }
      end
    end
  end
end
