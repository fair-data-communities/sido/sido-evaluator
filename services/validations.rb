module Sorbet
  module DefaultEvaluators
    module Validations
      # Returns whether a value is valid.
      # A value is valid if it is not null, or blank if it's a string.
      # Furthermore, if allowed_values array is specified, the value must be
      # present in any of the items of allowed_values.
      def valid_value?(value, allowed_values = nil)
        return false if value.nil?
        if value.is_a?(String)
          return false if value.strip == ""
        end

        if allowed_values.nil?
          return true
        end

        allowed_values.include?(value)
      end
    end
  end
end
