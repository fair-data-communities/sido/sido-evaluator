require_relative "./metadata"
require_relative "./sido_definitions"
require_relative "./planning_dataset"
require_relative "./validations"

module Sorbet
  module DefaultEvaluators
    # Interpretes metadata files. For now, this is more of an XML parser
    # than a RDF parser. However, this behavious may change in the future.
    class PlanningMetadata < Metadata
      include SidODefinitions
      include Validations

      def dataset_name
        RdfName.new(space: METADATA_PLANNING, local: "")
      end

      def extend_dataset(dataset)
        dataset.extend PlanningDataset
      end

      def expected_mapping
        ["id", "name", "curve", "startDate", "endDate", "contact", "status"]
      end

      def mapping_space
        METADATA_PLANNING_MAPPINGS
      end

      def identification_info
        {
          dataset_present: all_datasets.count.positive?,
          valid_dataset_present: datasets.count.positive?,
          distribution_present: dataset.distributions&.count&.positive? || false,
          distribution_contains_url: valid_value?(dataset.distribution&.endpoint_url)
        }
      end
    end
  end
end
