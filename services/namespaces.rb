module Sorbet
  module DefaultEvaluators
    module StandardNamespaces
      NS_DC     = "http://purl.org/dc/elements/1.1/"
      NS_DCAT   = "http://www.w3.org/ns/dcat"
      NS_FOAF   = "http://www.w3.org/2002/07/owl"
      NS_OWL    = "http://www.w3.org/2002/07/owl"
      NS_RDF    = "http://www.w3.org/1999/02/22-rdf-syntax-ns"
      NS_RDFS   = "http://www.w3.org/2000/01/rdf-schema"
      NS_PPB    = "http://purplepolarbear.nl/ppbxml"
    end
  end
end
