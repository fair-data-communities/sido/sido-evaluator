require_relative "./sido_definitions"
require_relative "./namespaces"
require_relative "./validations"

module Sorbet
  module DefaultEvaluators
    module PlanningDistribution
      include SidODefinitions
      include StandardNamespaces
      include Validations

      attr_accessor :dataset

      def protocol_info
        result = {
          application_protocol: nil,
          security_protocol: nil,
          file_format: nil
        }

        if endpoint_url.start_with?("https")
          result[:application_protocol] = :https
        elsif endpoint_url.start_with?("http")
          result[:application_protocol] = :http
        end

        wfs_predicate = RdfName.new(space: NS_DCAT, local: 'conformsTo')
        wfs = RdfName.new(space: "http://www.opengis.net/def/serviceType/ogc/wfs/2.0.0")
        if match_names(wfs_predicate, wfs)
          result[:file_format] = :xml
        end

        authentication_predicate = RdfName.new(space: METADATA_PLANNING, local: 'Authentication')
        authentication = dataset.contains_predicate(authentication_predicate)
        if authentication&.name&.space == METADATA_PLANNING
          authentication_method = authentication.name.local
          if authentication_method == "Authentication-clientserverssl"
            result[:security_protocol] = :clientserverssl
          end
          if authentication_method == "Authentication-none"
            result[:security_protocol] = :none
          end
        end

        result
      end

      def endpoint_url
        return @endpoint_url if @endpoint_url

        endpoint_predicate = RdfName.new(space: NS_DCAT, local: 'accessURL')
        @endpoint_url = contains_predicate(endpoint_predicate)&.name&.to_s || ""
      end
    end
  end
end
